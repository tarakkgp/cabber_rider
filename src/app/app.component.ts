import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//import { AddmoneyPage } from '../pages/addmoney/addmoney';
import { HomePage } from '../pages/home/home';
//import { ListPage } from '../pages/list/list';
import { LoginPage } from '../pages/login/login';
import { WalletPage } from '../pages/wallet/wallet';
import { HelpPage } from '../pages/help/help';
import { ProfilePage } from '../pages/profile/profile';
import { ReferralcodePage } from '../pages/referralcode/referralcode';
import { MytripsPage } from '../pages/mytrips/mytrips';
import { ReferPage } from '../pages/refer/refer';
//import { NotificationPage } from '../pages/notification/notification';
import { TranslateService } from '../../node_modules/@ngx-translate/core';

  

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
    
  @ViewChild(Nav) nav: Nav;
  rootPage: any = LoginPage;
  pages: Array<{title: string, component: any}>;
    
  constructor(private platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen, public translate:TranslateService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      //{ title: 'Home', component: HomePage },
     // { title: 'List', component: ListPage }
    ];

  }

initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
       this.translate.setDefaultLang('en');
      this.translate.use('en');
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  
  homePage() {
    this.nav.setRoot(HomePage);
  }
  mytripsPage() {
    this.nav.setRoot(MytripsPage);
  }
 wallet() {
    this.nav.setRoot(WalletPage);
  }
  helpPage() {
    this.nav.setRoot(HelpPage);
  }
   referPage() {
    this.nav.setRoot(ReferPage);
  }
   loginPage() {
    this.nav.setRoot(LoginPage);
  }
  profilePage() {
    this.nav.setRoot(ProfilePage);
  }
  referralcodePage() {
    this.nav.setRoot(ReferralcodePage);
  }
}
