import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

import {BookridePage } from '../bookride/bookride';
@Component({
  selector: 'page-loading',
  templateUrl: 'loading.html'
})

export class LoadingPage {
	
constructor(public navCtrl: NavController, public viewCtrl: ViewController) {}
  bookride(){
    this.navCtrl.setRoot(BookridePage);
    }
  dismiss() {
		this.viewCtrl.dismiss();
	}
}
