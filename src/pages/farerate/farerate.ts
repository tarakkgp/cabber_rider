import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { HomePage } from '../home/home';
@Component({
  selector: 'page-farerate',
  templateUrl: 'farerate.html'
})
export class FareratePage {

  constructor(public navCtrl: NavController) {

  }
 home(){
    this.navCtrl.setRoot(HomePage);
    }
}