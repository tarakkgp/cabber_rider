import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChoosecabPage } from '../choosecab/choosecab';

declare var google;

@Component({
	selector: 'page-location',
	templateUrl: 'location.html'
})

export class LocationPage {
	@ViewChild('map') mapElement: ElementRef;
	map: any;

	constructor(public navCtrl: NavController) {

	}

	ionViewDidLoad() {
		let latLng = new google.maps.LatLng(20.5937, 78.9629);
		this.loadMap(latLng);
	}

	loadMap(latLng) {
		let mapOptions = {
			center: latLng,
			zoom: 15,
			mapTypeControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
	}

	choosecabPage() {
		this.navCtrl.push(ChoosecabPage);
	}

}
