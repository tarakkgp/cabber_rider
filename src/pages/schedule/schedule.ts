import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';


import {LoadingPage } from '../loading/loading';
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html'
})
export class SchedulePage {

  constructor(public navCtrl: NavController, public viewCtrl: ViewController) {

  }
  loading(){
    this.navCtrl.push(LoadingPage);
    }
  dismiss() {
		this.viewCtrl.dismiss();
	}
}
