import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { FareratePage } from '../farerate/farerate';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-bookride',
  templateUrl: 'bookride.html'
})
export class BookridePage {
  private viewType: string;
  constructor(public navCtrl: NavController) {

  }
   
    setViewType(vt) {
    this.viewType = vt;
  }
    
   farerate(){
    this.navCtrl.push(FareratePage);
    }
    
    home(){
    this.navCtrl.setRoot(HomePage);
    }
}
